///////////////////////////////////////////////////////////
//  Invoker.cpp
//  Implementation of the Class Invoker
//  Created on:      08-nov.-2018 21:05:19
//  Original author: p482457
///////////////////////////////////////////////////////////

#include <iostream>

#include "Invoker.h"


Invoker::Invoker()
{
}

Invoker::~Invoker()
{
}

void Invoker::execute(CmdPtr & cmd)
{
	// A COMPLETER:
    cmd->execute();	// Executer la commande
    m_cmdDone.push_back(cmd); // Stocker le pointeur dans la liste des commandes faites

}

void Invoker::undo()
{
	// A COMPLETER:

    if(m_cmdDone.size() != 0) // Verifier que la liste des commandes faites n'est pas vide
    {							// Si elle n'est pas vide:
        CmdPtr cmd = m_cmdDone.back();  //Recuperer la derniere commande faite
        cmd->cancel();					//Annuler la commande
        m_cmdDone.pop_back();			// Retirer la derniere commande de la liste des commandes faites
        m_cmdUndone.push_back(cmd);    // Ajouter la commande a la liste des commandes defaites
    }

}

void Invoker::redo()
{
	// A COMPLETER:

    
    if(m_cmdUndone.size() != 0)  // Verifier que la liste des commandes defaites n'est pas vide
    {                           // Si elle n'est pas vide:
        CmdPtr cmd = m_cmdUndone.back();
      //  execute(cmd);
        cmd->execute();                 //Executer la commande
        m_cmdUndone.pop_back();        //  Retirer la derniere commande de la liste des commandes defaites
        m_cmdDone.push_back(cmd);     // Ajouter la commande a la liste des commandes faites
    }
    
}
