///////////////////////////////////////////////////////////
//  SelectPrimitiveCmd.cpp
//  Implementation of the Class SelectPrimitiveCmd
//  Created on:      08-nov.-2018 21:05:02
//  Original author: p482457
///////////////////////////////////////////////////////////

#include "SelectPrimitiveCmd.h"
#include "VisitorPrimitiveSelector.h"

SelectPrimitiveCmd::SelectPrimitiveCmd(PRIMITIVE_TYPE typ, Object3DAbs & obj3d)
	: m_type(typ), m_obj3d(obj3d)
{
}

SelectPrimitiveCmd::~SelectPrimitiveCmd()
{
}

void SelectPrimitiveCmd::cancel()
{
    // A COMPLETER:
    //m_selectObjContainer.clear();
    clearSelectObjects();   // Vider le conteneur local a la commande d'objets selectionnes
   
}


void SelectPrimitiveCmd::execute()
{
    // A COMPLETER:
	VisitorPrimitiveSelector VisitorPrimitive(m_type);         // Construire un visiteur de selection
    m_obj3d.accept(VisitorPrimitive);                         // Appliquer le visiteur a l'objet 3d
    VisitorPrimitive.getSelectObjects(m_selectObjContainer); // Recuperer les objets selectionnes dans le conteneur local de
                                                            //la commande
}

void SelectPrimitiveCmd::getSelectObjects(Obj3DIteratorContainer & objContainer)
{
	// A COMPLETER:
    for(auto it = m_selectObjContainer.begin() ; it != m_selectObjContainer.end() ; it++ )
    objContainer.push_back(*it);         // Transferer les objets selectionnes du conteneur local a la commande
                                        // vers le conteneur fourni en argument
}
