///////////////////////////////////////////////////////////
//  VisitorPrimitiveSelector.cpp
//  Implementation of the Class VisitorPrimitiveSelector
//  Created on:      07-nov.-2018 21:20:38
//  Original author: p482457
///////////////////////////////////////////////////////////

#include <exception>

#include "VisitorPrimitiveSelector.h"
#include "Cube.h"
#include "Cylinder.h"
#include "Sphere.h"
#include "Object3DComposite.h"
#include "TransformedObject3D.h"

VisitorPrimitiveSelector::VisitorPrimitiveSelector(PRIMITIVE_TYPE typ)
	: m_type(typ)
{
}

VisitorPrimitiveSelector::~VisitorPrimitiveSelector()
{
}

void VisitorPrimitiveSelector::visitCube(class Cube& cub)
{
	// A COMPLETER:
    if(m_currentObjStack.size() != 0)           // Verifier que la pile d'objets courants n'est pas vide
    {
        if(m_type == PRIMITIVE_TYPE:: Cube_t)    // Verifier que le type de la primitive est bien celui recherchee
        {
            m_selectObjContainer.push_back(m_currentObjStack.back());     // Si oui, ajouter la primitive dans les objets
                                                                         //selectionnes
        }
    }
}

void VisitorPrimitiveSelector::visitCylinder(class Cylinder& cyl)
{
	// A COMPLETER:
    if(m_currentObjStack.size() != 0)           // Verifier que la pile d'objets courants n'est pas vide
    {
        if(m_type == PRIMITIVE_TYPE:: Cylinder_t)  // Verifier que le type de la primitive est bien celui recherchee
        {
            m_selectObjContainer.push_back(m_currentObjStack.back());     // Si oui, ajouter la primitive dans les objets
                                                                         //selectionnes
        }
    }
}

void VisitorPrimitiveSelector::visitObjComposite(const Object3DComposite& comp)
{
	throw(std::invalid_argument("VisitorPrimitiveSelector cannot process const Objects"));
}

void VisitorPrimitiveSelector::visitObjComposite(class Object3DComposite& comp)
{
	// A COMPLETER:
    
      for (auto it = comp.begin(); it != comp.end(); it++) // Iterer sur les enfants du composite
    {
        m_currentObjStack.push_back(it);                 // Stocker l'enfant sur la pile des objets courants
        it->accept(*this);                              // Traiter l'enfant
        m_currentObjStack.pop_back();                  // Retirer l'enfant de sur la pile
        
    }
}

void VisitorPrimitiveSelector::visitPrimitive(const class PrimitiveAbs& prim)
{
	throw(std::invalid_argument("VisitorPrimitiveSelector cannot process const Objects"));
}

void VisitorPrimitiveSelector::visitPrimitive(class PrimitiveAbs& prim)
{
	// Type de primitive inconnue... cette primitive n'est pas selectionnee
}

void VisitorPrimitiveSelector::visitSphere(class Sphere& sph)
{
	// A COMPLETER:

    if(m_currentObjStack.size() != 0)           // Verifier que la pile d'objets courants n'est pas vide
    {
        if(m_type == PRIMITIVE_TYPE:: Sphere_t) // Verifier que le type de la primitive est bien celui recherchee
        {
            m_selectObjContainer.push_back(m_currentObjStack.back());     // Si oui, ajouter la primitive dans les objets
                                                                         //selectionnes
        }
    }
}

void VisitorPrimitiveSelector::visitTransformedObj(class TransformedObject3D& tobj)
{
	// A COMPLETER:
    tobj.accept(*this);  // Deleguer le traitement a la primitive contenue dans le decorateur
    
}

void VisitorPrimitiveSelector::getSelectObjects(Obj3DIteratorContainer & objContainer)
{
	// A COMPLETER:
	objContainer = m_selectObjContainer;     // Transferer les objets selectionnes du conteneur local au visiteur
                                            // vers le conteneur fourni en argument

}
