
#include "Torus.h"


Torus::Torus(const Point3D& pt, float r1, float r2, float ht)
: PrimitiveAbs(pt)
{
    if (r1 < 0.0 || r2 < 0.0 || ht < 0.0)
    throw std::range_error("Invalid parameter value for cylinder. Must be larger than 0");
    
    // A Completer...
    else
    m_dimensions[0]= r1;
    m_dimensions[1]= r2;
    m_dimensions[2]= ht;
    
}

Torus::~Torus(){
}

Torus * Torus::clone() const
{
    // A Completer...
    if (this != nullptr)
    {
        Torus* TorusClone = new Torus(getCenter(), m_dimensions[0], m_dimensions[1],m_dimensions[2]);
        return TorusClone;
    }
    else
    return nullptr;
}


size_t Torus::getNbParameters() const {
    
    // A Completer...
    // return 0 ;
    return 3 ;
}

PrimitiveParams Torus::getParameters() const
{
    // A Completer...
    //return PrimitiveParams();
    
    PrimitiveParams parametres;
    for (int i = 0 ; i < getNbParameters(); i++)
    {
        parametres.push_back(m_dimensions[i]);
    }
    return parametres;
}

void Torus::setParameter(size_t pIndex, float pValue){
    if (pIndex > 1 || pIndex < 0)
    throw std::range_error("Invalid parameter index for Torus. Must be between 0 and 1");
    
    if (pValue < 0.0)
    throw std::range_error("Invalid parameter value for Torus. Must be larger than 0");
    
    // A Completer...
    m_dimensions[pIndex] = pValue;
}

std::ostream & Torus::toStream(std::ostream & o) const
{
    return o << "Torus:  center = " << m_center
    << "; r1 = " << m_dimensions[0]
    << "; r2 = " << m_dimensions[1] << ";"
    << "; ht = " << m_dimensions[2] << ";";
}
