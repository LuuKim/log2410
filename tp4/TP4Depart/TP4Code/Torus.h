
#if !defined TORUS_H
#define TORUS_H

#include "PrimitiveAbs.h"

class Torus : public PrimitiveAbs
{
    
    public:
    Torus(const Point3D& pt, float r1, float r2, float ht);
    virtual ~Torus();
    virtual Torus* clone() const;
    
    virtual size_t getNbParameters() const;
    virtual PrimitiveParams getParameters() const;
    virtual void setParameter(size_t pIndex, float pValue);
    
    private:
    virtual std::ostream& toStream(std::ostream& o) const;
    
    float m_dimensions[3];
};
#endif


